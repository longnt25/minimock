﻿using Microsoft.EntityFrameworkCore;
using StudentMiniMock.Data.Entities;
using System;

namespace StudentMiniMock.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        public DbSet<Student> Students { get; set; }
      
    }
}
