﻿using Microsoft.AspNetCore.Mvc;
using StudentMiniMock.Business.Repository;
using StudentMiniMock.Data;
using StudentMiniMock.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentMiniMock.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IStudentRepository _studentRepository;

        public StudentController(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }

        // GET: student/<StudentController>
        [HttpGet]
        public async Task<List<Student>> Get() => await _studentRepository.Get();

        // GET: student/<StudentController>
        [HttpGet("{id}")]
        public async Task<List<Student>> Get(int? id) => await _studentRepository.Get(id);

        // POST student/<StudentController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Student student)
        {
            await _studentRepository.Create(student);
            return Ok();
        }

        // PUT student/<StudentController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromBody] Student student)
        {
            await _studentRepository.Update(student);
            return Ok();
        }

        // DELETE student/<StudentController>/5
        [HttpDelete("{id}")]
        public async Task<bool> Delete(int id) => await _studentRepository.Delete(id);
    }
}
