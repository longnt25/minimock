using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using StudentMiniMock.Data;
using StudentMiniMock.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StudentMiniMock
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            SeedData(host);
            host.Run();
        }

        public static void SeedData(IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<DataContext>();
                var studentList = new List<Student>
                {
                    new Student
                    {
                        Id = 1,
                        Name = "Luke",
                        Age = 18
                    },
                    new Student
                    {
                        Id = 2,
                        Name = "Lux",
                        Age = 19
                    },
                    new Student
                    {
                        Id = 3,
                        Name = "Kid",
                        Age = 18
                    },
                    new Student
                    {
                        Id = 4,
                        Name = "Haru",
                        Age = 20
                    },
                };
                context.Students.AddRange(studentList);
                context.SaveChanges();
            }
        }


        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
