﻿using Microsoft.EntityFrameworkCore;
using StudentMiniMock.Data;
using StudentMiniMock.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentMiniMock.Business.Repository.Implement
{
    public class StudentRepository : IStudentRepository
    {
        private DataContext _context;
        public StudentRepository(DataContext dataContext)
        {
            _context = dataContext;
        }

        public async Task<Student> Create(Student student)
        {
            _context.Students.Add(student);
            await _context.SaveChangesAsync();

            return student;
        }

        public async Task<bool> Delete(int id)
        {
            var student = await _context.Students.FirstOrDefaultAsync(item => item.Id == id);
            _context.Students.Remove(student);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<List<Student>> Get(int? id) => await _context.Students.Where(s => s.Id == id).ToListAsync();

        public async Task<List<Student>> Get() => await _context.Students.ToListAsync();

        public async Task<Student> Update(Student student)
        {
            var std = await _context.Students.FirstOrDefaultAsync(s => s.Id == student.Id);
            if (std == null)
            {
                throw new ArgumentNullException();
            }
            std.Name = student.Name;
            std.Age = student.Age;
            _context.Students.Update(std);
            await _context.SaveChangesAsync();

            return std;
        }
    }
}
