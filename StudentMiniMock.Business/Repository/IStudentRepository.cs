﻿using StudentMiniMock.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace StudentMiniMock.Business.Repository
{
    public interface IStudentRepository
    {
        public Task<List<Student>> Get();
        public Task<List<Student>> Get(int? id);
        public Task<Student> Create(Student student);
        public Task<Student> Update(Student student);
        public Task<bool> Delete(int id);
    }
}
